
const fs = require ('fs');
const fetch = require('node-fetch');
const readline = require('readline');
var jsonfile=require('json');

var lector = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});


function opcionMenu(opcion){
    switch(opcion){
        case '1': console.log('Opcion 1');
        fetch('https://mindicador.cl/api').then(status).then(obtenerJson).then((datos)=>{
            fs.writeFile(jsonfile,JSON.stringify(datos,null,2),(err)=>{console.error(err)}); 
        }).catch(console.error);
        break;
        case '2': getFile('./datos.json').then(JSON.parse).then((datos)=>{
            obtenerPromedio(datos)}).catch(console.error);
        
        break;
        case '3': 
        
        break;
        case '4': process.exit(0);
        break;
        default: 
        console.log('Opcion no encontrada');
        break;
    }
//leerOpcion().then(  (opcion)=>{opcionMenu(opcion);} ).catch(console.error);
}

const status = response =>{
    if(response.status >= 200 && response.status < 300){
        return Promise.resolve(response);
    }
    return Promise.reject(new Error(response.statusText));
};
const obtenerJson = response => {
    return response.json();
};
const getFile = fileName => {
    return new Promise((resolve,reject)=>{
        fs.readFile(fileName,(err,data)=>{
            if(err){
                reject(err);
                return;
            }
            resolve(data);
        });
    });
}
function obtenerPromedio(json){
    //console.log(json['uf'].valor);
    var info="PROMEDIOS \n";
    for (var elem in json){
        //console.log(elem);
        if(elem!='version' && elem!='autor' && elem!='fecha'){
            info+=elem+": "+json[elem].valor+"\n";
        }
    }
    console.log(info);
    //leerOpcion().then(  (opcion)=>{opcionMenu(opcion);} ).catch(console.error);
}
// const writeFile = fileName => {
//     return new Promise((resolve,reject)=>{
//         fs.writeFile(fileName, (err,data)=>{
//             if(err){
//                 reject(err);
//                 return;
//             }
//             resolve(data);
//         });
//     });
// };

var leerOpcion = ()=>{
    return new Promise((resolve,reject)=>{
        console.log('Menu');
        console.log('1.- Actualizar Datos');
        console.log('2.- Ultimos 5 promedios');
        console.log('3.- Minimo y maximo ultimos 5 archivos');
        console.log('4.- Salir');    
        lector.question('Escriba su opcion: ', opcion =>{
            resolve(opcion);
        });
    });
}


/*fetch('https://mindicador.cl/api').then(status).then(obtenerJson).then((datos)=>{fs.writeFile('indicadores.json',JSON.stringify(datos),(err)=>{console.error(err)}); 
}).catch(console.error);*/

leerOpcion().then((opcion)=>{opcionMenu(opcion);}).catch(console.error);